var gulp = require('gulp'),
    sass = require('gulp-sass');

gulp.task('scss', function(){
    return gulp.src('app/scss/**/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('app/css'))
});

// gulp.task('try', function(){
//     return gulp.src('source-files')
//     .pipe(plugin())
//     .pipe(gulp.dest('folder'))
// });

